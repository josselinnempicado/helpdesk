﻿USE Helpdesk

                        --USUARIOS--
INSERT INTO TipoUsuario ( nombreTipoUsuario) 
VALUES ( 'Administrador'), ( 'Tecnico'), ('Cliente')

INSERT INTO Usuario ( nombreUsuario, contrasena, correo, esvip, idTipoUsuario)
VALUES ('Administrador','HelpDesk2021','invenio2021@gmail.com',0,0)

INSERT INTO Usuario ( nombreUsuario, contrasena, correo, esvip,  idTipoUsuario)
VALUES ('Juan Tecnico','HelpDeskTecJuan12','invenio2021+tecnico1@gmail.com',0,1),
	    ('Enrique Tecnico','HelpDeskTecEnrique12','invenio2021+tecnico2@gmail.com',0,1)

INSERT INTO Usuario ( nombreUsuario, contrasena, correo, esvip,  idTipoUsuario)
VALUES ('Daniel Tecnico','HelpDeskTecDaniel12','invenio2021+tecnico3@gmail.com',0,1),
		('Maria Tecnica','HelpDeskTecMaria12','invenio2021+tecnico4@gmail.com',0,1)

INSERT INTO Usuario ( nombreUsuario, contrasena, correo, esvip,  idTipoUsuario)
VALUES ('Sandra Tecnica','HelpDeskTecSandra12','invenio2021+tecnico5@gmail.com',0,1),
		('Karla Tecnica','HelpDeskTecKarla12','invenio2021+tecnico6@gmail.com',0,1)

INSERT INTO Usuario ( nombreUsuario, contrasena, correo, esvip, idTipoUsuario)
VALUES ('Patricia Cliente','HelpDeskCli12','invenio2021+cliente1@gmail.com',0,2)

INSERT INTO Usuario ( nombreUsuario, contrasena, correo, esvip, idTipoUsuario)
VALUES ('Edgardo Cliente','HelpDeskCli212','invenio2021+cliente2@gmail.com',1,2)
                     --PRIORIDADES--
INSERT INTO Prioridad (nombrePrioridad ) VALUES ('Baja'),('Media'), ('Alta')
                    --ESTADO--
INSERT INTO Estado (nombreEstado ) VALUES ('Abierto'),('Cerrado')
                  --ESPECIALIDADES--
INSERT INTO Especialidad (nombreEspecialidad) VALUES ('Sistema Matrícula'), ('Sistema Administrativo'), ('Equipos Tecnológicos') 
                   --ESPECIALIDADtECNICO--
INSERT INTO EspecialidadTecnico (idUsuario,idEspecialidad) VALUES (1,0),(2,1),(3,2),(4,0),(5,1),(6,2),(4,2),(5,0),(6,1)

