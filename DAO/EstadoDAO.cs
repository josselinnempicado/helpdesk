﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using TO;

namespace DAO
{
    public class EstadoDAO
    {
        private static Conexion conexion;

        static EstadoDAO()
        {
            conexion = new Conexion();
        }
        public static EstadoTO TraerEstado(string nombreEstado)
        {
            //Acceso a la base de datos
            String query = "select * from Estado where nombreEstado='" + nombreEstado + "'";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                conexion.cerrar();
                return null;
            }
            reader.Read();
            EstadoTO estado = new EstadoTO(Convert.ToInt32(reader["idEstado"]), Convert.ToString(reader["nombreEstado"]));

            conexion.cerrar();
            return estado;
        }
        public static EstadoTO TraerEstado(int idEstado)
        {
            //Acceso a la base de datos
            String query = "select * from Estado where idEstado=" + idEstado;
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                conexion.cerrar();
                return null;
            }
            reader.Read();
            EstadoTO estado = new EstadoTO(Convert.ToInt32(reader["idEstado"]), Convert.ToString(reader["nombreEstado"]));

            conexion.cerrar();
            return estado;
        }
    }
}

