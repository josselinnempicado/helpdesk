﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using TO;

namespace DAO
{
    public class TicketDAO
    {
        private static Conexion conexion;

        static TicketDAO()
        {
            conexion = new Conexion();
        }

        public static TicketTO TraerTicket(int idticket)
        {
            String query = "SELECT * FROM ticket WHERE idticket=" + idticket;
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                string asunto = Convert.ToString(reader["asunto"]);
                DateTime ultimaActualizacion = Convert.ToDateTime(reader["ultimaActualizacion"]);
                DateTime fechaCreacion = Convert.ToDateTime(reader["fechaCreacion"]);
                object fechasolucionobject = reader["fechaSolucion"];
                DateTime fechaSolucion = new DateTime();
                if (!DBNull.Value.Equals(fechasolucionobject))
                {
                    fechaSolucion = Convert.ToDateTime(reader["fechaSolucion"]);
                }
                int idCliente = Convert.ToInt32(reader["idCliente"]);
                int idTecnico = Convert.ToInt32(reader["idTecnico"]);
                int idPrioridad = Convert.ToInt32(reader["idPrioridad"]);
                int idEstado = Convert.ToInt32(reader["idEstado"]);
                int idEspecialidad = Convert.ToInt32(reader["idEspecialidad"]);

                conexion.cerrar();

                UsuarioTO cliente = UsuarioDAO.TraerUsuario(idCliente);
                UsuarioTO tecnico = UsuarioDAO.TraerUsuario(idTecnico);
                PrioridadTO prioridad = PrioridadDAO.TraerPrioridad(idPrioridad);
                EstadoTO estado = EstadoDAO.TraerEstado(idEstado);
                EspecialidadTO especialidad = EspecialidadDAO.TraerEspecialidad(idEspecialidad);

                TicketTO ticket = new TicketTO(idticket, asunto, ultimaActualizacion, fechaCreacion, fechaSolucion, cliente, tecnico, prioridad, estado, especialidad);

                return ticket;
            }
            conexion.cerrar();
            return null;
        }
        public static List<TicketTO> TraerTodosLosTickets()
        {
      

            String query = "SELECT * FROM ticket";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            List<TicketTO> tickets = new List<TicketTO>();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int idticket = Convert.ToInt32(reader["idticket"]);
                    string asunto = Convert.ToString(reader["asunto"]);
                    DateTime ultimaActualizacion = Convert.ToDateTime(reader["ultimaActualizacion"]);
                    DateTime fechaCreacion = Convert.ToDateTime(reader["fechaCreacion"]);
                    object fechasolucionobject = reader["fechaSolucion"];
                    DateTime fechaSolucion = new DateTime();
                    if (!DBNull.Value.Equals(fechasolucionobject))
                    {
                        fechaSolucion = Convert.ToDateTime(reader["fechaSolucion"]);
                    }
                    int idCliente = Convert.ToInt32(reader["idCliente"]);
                    int idTecnico = Convert.ToInt32(reader["idTecnico"]);
                    int idPrioridad = Convert.ToInt32(reader["idPrioridad"]);
                    int idEstado = Convert.ToInt32(reader["idEstado"]);
                    int idEspecialidad = Convert.ToInt32(reader["idEspecialidad"]);

                    UsuarioTO cliente = UsuarioDAO.TraerUsuario(idCliente);
                    UsuarioTO tecnico = UsuarioDAO.TraerUsuario(idTecnico);
                    PrioridadTO prioridad = PrioridadDAO.TraerPrioridad(idPrioridad);
                    EstadoTO estado = EstadoDAO.TraerEstado(idEstado);
                    EspecialidadTO especialidad = EspecialidadDAO.TraerEspecialidad(idEspecialidad);

                    TicketTO ticket = new TicketTO(idticket, asunto, ultimaActualizacion, fechaCreacion, fechaSolucion, cliente, tecnico, prioridad, estado, especialidad);

                    tickets.Add(ticket);
                }
            }

            conexion.cerrar();
            return tickets;
        }
        public static UsuarioTO TraerTecnicoConEspecialidad(EspecialidadTO especialidad)
        {
            String query = "SELECT Usuario.idUsuario " +
                "FROM Usuario, TipoUsuario, Especialidad, EspecialidadTecnico " +
                "WHERE usuario.idTipoUsuario = TipoUsuario.idTipoUsuario and " +
                "usuario.idUsuario = EspecialidadTecnico.idUsuario and " +
                "especialidad.idespecialidad = especialidadTecnico.idespecialidad and " +
                "nombreTipoUsuario = 'Tecnico' and " +
                "nombreEspecialidad = 'Sistema Administrativo'";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            List<UsuarioTO> tecnicos = new List<UsuarioTO>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int idTecnico = Convert.ToInt32(reader["idUsuario"]);

                    UsuarioTO Tecnico = UsuarioDAO.TraerUsuario(idTecnico);

                    tecnicos.Add(Tecnico);
                }

                Random rnd = new Random();
                int i = rnd.Next(tecnicos.Count);

                conexion.cerrar();
                return tecnicos.ElementAt(i);

            }
            conexion.cerrar();
            return null;

        }

        public static TicketTO CrearNuevoTicket(
            String asunto,
            DateTime ultimaActualizacion,
            DateTime fechaCreacion,
            TO.UsuarioTO cliente,
            TO.UsuarioTO tecnico,
            TO.PrioridadTO prioridad,
            TO.EstadoTO estado,
            TO.EspecialidadTO especialidad)
        {
            conexion.abrir();
            SqlCommand command = new SqlCommand(null, conexion.sqlConnection);
            command.CommandText = "INSERT INTO Ticket (asunto,fechaCreacion,ultimaActualizacion,idCliente,idTecnico,idPrioridad,idEstado,idEspecialidad)" +
                "VALUES(@asunto,@fechaCreacion,@ultimaActualizacion,@idCliente,@idTecnico,@idPrioridad,@idEstado,@idEspecialidad);" +
                "select scope_identity();";
            SqlParameter paramAsunto = new SqlParameter("@asunto", SqlDbType.Char, 50);
            SqlParameter paramFechaCreacion = new SqlParameter("@fechaCreacion", SqlDbType.DateTime);
            SqlParameter paramUltimaActualizacion = new SqlParameter("@ultimaActualizacion", SqlDbType.DateTime);
            SqlParameter paramIdCliente = new SqlParameter("@idCliente", SqlDbType.Int);
            SqlParameter paramIdTecnico = new SqlParameter("@idTecnico", SqlDbType.Int);
            SqlParameter paramIdPrioridad = new SqlParameter("@idPrioridad", SqlDbType.Int);
            SqlParameter paramIdEstado = new SqlParameter("@idEstado", SqlDbType.Int);
            SqlParameter paramIdEspecialidad = new SqlParameter("@idEspecialidad", SqlDbType.Int);
            paramAsunto.Value = asunto;
            paramFechaCreacion.Value = fechaCreacion;
            paramUltimaActualizacion.Value = ultimaActualizacion;
            paramIdCliente.Value = cliente.idUsuario;
            paramIdTecnico.Value = tecnico.idUsuario;
            paramIdPrioridad.Value = prioridad.idPrioridad;
            paramIdEstado.Value = estado.idEstado;
            paramIdEspecialidad.Value = especialidad.idEspecialidad;
            command.Parameters.Add(paramAsunto);
            command.Parameters.Add(paramFechaCreacion);
            command.Parameters.Add(paramUltimaActualizacion);
            command.Parameters.Add(paramIdCliente);
            command.Parameters.Add(paramIdTecnico);
            command.Parameters.Add(paramIdPrioridad);
            command.Parameters.Add(paramIdEstado);
            command.Parameters.Add(paramIdEspecialidad);
            command.Prepare();

            int idticket = Convert.ToInt32(command.ExecuteScalar());
            conexion.cerrar();
            return new TicketTO(idticket, asunto, ultimaActualizacion, fechaCreacion, new DateTime(), cliente, tecnico, prioridad, estado, especialidad);

        }


        public static List<TicketTO> TraerTodosMisTicketsCliente(int idCliente)
        {
            String query = "SELECT * FROM ticket WHERE idCliente=" + idCliente;
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            List<TicketTO> tickets = new List<TicketTO>();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int idticket = Convert.ToInt32(reader["idticket"]);
                    string asunto = Convert.ToString(reader["asunto"]);
                    DateTime ultimaActualizacion = Convert.ToDateTime(reader["ultimaActualizacion"]);
                    DateTime fechaCreacion = Convert.ToDateTime(reader["fechaCreacion"]);
                    object fechasolucionobject = reader["fechaSolucion"];
                    DateTime fechaSolucion = new DateTime();
                    if (!DBNull.Value.Equals(fechasolucionobject))
                    {
                        fechaSolucion = Convert.ToDateTime(reader["fechaSolucion"]);
                    }
                    int idTecnico = Convert.ToInt32(reader["idTecnico"]);
                    int idPrioridad = Convert.ToInt32(reader["idPrioridad"]);
                    int idEstado = Convert.ToInt32(reader["idEstado"]);
                    int idEspecialidad = Convert.ToInt32(reader["idEspecialidad"]);

                    UsuarioTO cliente = UsuarioDAO.TraerUsuario(idCliente);
                    UsuarioTO tecnico = UsuarioDAO.TraerUsuario(idTecnico);
                    PrioridadTO prioridad = PrioridadDAO.TraerPrioridad(idPrioridad);
                    EstadoTO estado = EstadoDAO.TraerEstado(idEstado);
                    EspecialidadTO especialidad = EspecialidadDAO.TraerEspecialidad(idEspecialidad);

                    TicketTO ticket = new TicketTO(idticket, asunto, ultimaActualizacion, fechaCreacion, fechaSolucion, cliente, tecnico, prioridad, estado, especialidad);

                    tickets.Add(ticket);
                }
            }

            conexion.cerrar();
            return tickets;
        }
        public static List<TicketTO> TraerTodosMisTicketsTecnico(int idTecnico)
        {
            String query = "SELECT * FROM ticket WHERE idTecnico=" + idTecnico;
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            List<TicketTO> tickets = new List<TicketTO>();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int idticket = Convert.ToInt32(reader["idticket"]);
                    string asunto = Convert.ToString(reader["asunto"]);
                    DateTime ultimaActualizacion = Convert.ToDateTime(reader["ultimaActualizacion"]);
                    DateTime fechaCreacion = Convert.ToDateTime(reader["fechaCreacion"]);
                    object fechasolucionobject = reader["fechaSolucion"];
                    DateTime fechaSolucion = new DateTime();
                    if (!DBNull.Value.Equals(fechasolucionobject))
                    {
                        fechaSolucion = Convert.ToDateTime(reader["fechaSolucion"]);
                    }
                    int idCliente = Convert.ToInt32(reader["idCliente"]);
                    int idPrioridad = Convert.ToInt32(reader["idPrioridad"]);
                    int idEstado = Convert.ToInt32(reader["idEstado"]);
                    int idEspecialidad = Convert.ToInt32(reader["idEspecialidad"]);

                    UsuarioTO cliente = UsuarioDAO.TraerUsuario(idCliente);
                    UsuarioTO tecnico = UsuarioDAO.TraerUsuario(idTecnico);
                    PrioridadTO prioridad = PrioridadDAO.TraerPrioridad(idPrioridad);
                    EstadoTO estado = EstadoDAO.TraerEstado(idEstado);
                    EspecialidadTO especialidad = EspecialidadDAO.TraerEspecialidad(idEspecialidad);

                    TicketTO ticket = new TicketTO(idticket, asunto, ultimaActualizacion, fechaCreacion, fechaSolucion, cliente, tecnico, prioridad, estado, especialidad);

                    tickets.Add(ticket);
                }
            }

            conexion.cerrar();
            return tickets;
        }
        public static List<TicketTO> TraerTodosLosTicketsConAlerta()
        {


            String query = "SELECT * FROM TICKET WHERE DATEDIFF(day,fechaCreacion, GETDATE()) > 6  and fechaSolucion is null";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            List<TicketTO> tickets = new List<TicketTO>();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int idticket = Convert.ToInt32(reader["idticket"]);
                    string asunto = Convert.ToString(reader["asunto"]);
                    DateTime ultimaActualizacion = Convert.ToDateTime(reader["ultimaActualizacion"]);
                    DateTime fechaCreacion = Convert.ToDateTime(reader["fechaCreacion"]);
                    object fechasolucionobject = reader["fechaSolucion"];
                    DateTime fechaSolucion = new DateTime();
                    if (!DBNull.Value.Equals(fechasolucionobject))
                    {
                        fechaSolucion = Convert.ToDateTime(reader["fechaSolucion"]);
                    }
                    int idCliente = Convert.ToInt32(reader["idCliente"]);
                    int idTecnico = Convert.ToInt32(reader["idTecnico"]);
                    int idPrioridad = Convert.ToInt32(reader["idPrioridad"]);
                    int idEstado = Convert.ToInt32(reader["idEstado"]);
                    int idEspecialidad = Convert.ToInt32(reader["idEspecialidad"]);

                    UsuarioTO cliente = UsuarioDAO.TraerUsuario(idCliente);
                    UsuarioTO tecnico = UsuarioDAO.TraerUsuario(idTecnico);
                    PrioridadTO prioridad = PrioridadDAO.TraerPrioridad(idPrioridad);
                    EstadoTO estado = EstadoDAO.TraerEstado(idEstado);
                    EspecialidadTO especialidad = EspecialidadDAO.TraerEspecialidad(idEspecialidad);

                    TicketTO ticket = new TicketTO(idticket, asunto, ultimaActualizacion, fechaCreacion, fechaSolucion, cliente, tecnico, prioridad, estado, especialidad);

                    tickets.Add(ticket);
                }
            }

            conexion.cerrar();
            return tickets;
        }

        public static void cerrarTicket(TicketTO ticket)
        {
            conexion.abrir();
            SqlCommand command = new SqlCommand("UPDATE Ticket SET idEstado = 1, fechaSolucion = getdate() WHERE idTicket=" + ticket.idTicket,
                conexion.sqlConnection);
            command.ExecuteNonQuery();
            conexion.cerrar();
        }
    }
}
