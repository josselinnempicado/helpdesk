﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TO;

namespace DAO
{
    public class BitacoraDAO
    {
        private static Conexion conexion;

        static BitacoraDAO()
        {
            conexion = new Conexion();
        }
        public static BitacoraTO CrearNuevaBitacora(

            DateTime fecha,
            String comentario,
            UsuarioTO usuario,
            TicketTO ticket)
        {
            conexion.abrir();
            SqlCommand command = new SqlCommand(null, conexion.sqlConnection);
            command.CommandText = "INSERT INTO Bitacora (fecha,comentario,idUsuario,idTicket)" +
                "VALUES (@fecha,@comentario,@idusuario,@idticket);" +
                "select scope_identity();";
            SqlParameter paramFecha = new SqlParameter("@fecha", SqlDbType.DateTime);
            SqlParameter paramComentario = new SqlParameter("@comentario", SqlDbType.Text, comentario.Length);
            SqlParameter paramIdusuario = new SqlParameter("@idusuario", SqlDbType.Int);
            SqlParameter paramIdticket = new SqlParameter("@idticket", SqlDbType.Int);
            paramFecha.Value = fecha;
            paramComentario.Value = comentario;
            paramIdusuario.Value = usuario.idUsuario;
            paramIdticket.Value = ticket.idTicket;
            command.Parameters.Add(paramFecha);
            command.Parameters.Add(paramComentario);
            command.Parameters.Add(paramIdusuario);
            command.Parameters.Add(paramIdticket);
            command.Prepare();

            int idBitacora = Convert.ToInt32(command.ExecuteScalar());
            conexion.cerrar();

            return new BitacoraTO(idBitacora, fecha, comentario, usuario, ticket);
        }
        public static List<BitacoraTO> TraerTodosLasBitacorasDeUnTicket(TicketTO ticket)
        {
            String query = "SELECT * FROM Bitacora WHERE idTicket= " + ticket.idTicket;
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            List<BitacoraTO> bitacoras = new List<BitacoraTO>();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int idBitacora = Convert.ToInt32(reader["idBitacora"]);
                    DateTime fecha = Convert.ToDateTime(reader["fecha"]);
                    string comentario = Convert.ToString(reader["comentario"]);
                    int idUsuario = Convert.ToInt32(reader["idUsuario"]);
                    UsuarioTO usuario = UsuarioDAO.TraerUsuario(idUsuario);
                    int idTicket = Convert.ToInt32(reader["idTicket"]);

                    BitacoraTO bitacora = new BitacoraTO(idBitacora, fecha, comentario, usuario, ticket);
                    bitacoras.Add(bitacora);
                }
            }
            conexion.cerrar();
            return bitacoras;
        }
    }
}

