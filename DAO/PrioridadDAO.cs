﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using TO;

namespace DAO
{
    public class PrioridadDAO
    {
        private static Conexion conexion;
        static PrioridadDAO()
        {
            conexion = new Conexion();
        }

        public static PrioridadTO TraerPrioridad(String nombrePrioridad)
        {
            //Acceso a la base de datos
            String query = "select * from Prioridad where nombrePrioridad='" + nombrePrioridad + "'";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            PrioridadTO prioridades = new PrioridadTO();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                conexion.cerrar();
                return null;
            }
            reader.Read();
            PrioridadTO prioridad = new PrioridadTO(Convert.ToInt32(reader["idPrioridad"]), Convert.ToString(reader["nombrePrioridad"]));

            conexion.cerrar();
            return prioridad;
        }

        public static PrioridadTO TraerPrioridad(int idPrioridad)
        {
            //Acceso a la base de datos
            String query = "select * from Prioridad where idPrioridad=" + idPrioridad;
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            PrioridadTO prioridades = new PrioridadTO();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                conexion.cerrar();
                return null;
            }
            reader.Read();
            PrioridadTO prioridad = new PrioridadTO(Convert.ToInt32(reader["idPrioridad"]), Convert.ToString(reader["nombrePrioridad"]));

            conexion.cerrar();
            return prioridad;
        }
    }
}
