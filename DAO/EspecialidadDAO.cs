﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using TO;

namespace DAO
{
    public class EspecialidadDAO
    {
        private static Conexion conexion;

        static EspecialidadDAO()
        {
            conexion = new Conexion();
        }

        public static List<EspecialidadTO> consultarEspecialidades()
        {
            String query = "select * from especialidad;";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            List<EspecialidadTO> especialidad = new List<EspecialidadTO>();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    EspecialidadTO especialidadconsultar = new EspecialidadTO(Convert.ToInt32(reader["idEspecialidad"]), Convert.ToString(reader["nombreEspecialidad"]));
                    especialidad.Add(especialidadconsultar);
                }
            }
            conexion.cerrar();

            return especialidad;
        }
        public static EspecialidadTO TraerEspecialidad(string nombreEspecialidad)
        {
            //Acceso a la base de datos
            String query = "select * from Especialidad where nombreEspecialidad='" + nombreEspecialidad + "'";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                conexion.cerrar();
                return null;
            }
            reader.Read();
            EspecialidadTO especialidad = new EspecialidadTO(Convert.ToInt32(reader["idEspecialidad"]), Convert.ToString(reader["nombreEspecialidad"]));

            conexion.cerrar();
            return especialidad;
        }

        public static EspecialidadTO TraerEspecialidad(int idEspecialidad)
        {
            //Acceso a la base de datos
            String query = "select * from Especialidad where idEspecialidad=" + idEspecialidad ;
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                conexion.cerrar();
                return null;
            }
            reader.Read();
            EspecialidadTO especialidad = new EspecialidadTO(Convert.ToInt32(reader["idEspecialidad"]), Convert.ToString(reader["nombreEspecialidad"]));

            conexion.cerrar();
            return especialidad;
        }
    }
}


