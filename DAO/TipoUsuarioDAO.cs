﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using TO;

namespace DAO
{
    public class TipoUsuarioDAO
    {
        private static Conexion conexion;

       static TipoUsuarioDAO()
        {
            conexion = new Conexion();
        }
        public static TipoUsuarioTO TraerTipoUsuario(string nombreTipoUsuario)
        {
            //Acceso a la base de datos
            String query = "select * from TipoUsuario where nombreTipoUsuario='" + nombreTipoUsuario + "'";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                conexion.cerrar();
                return null;
            }
            reader.Read();
            TipoUsuarioTO tipousuario = new TipoUsuarioTO(Convert.ToInt32(reader["idTipoUsuario"]), Convert.ToString(reader["nombreTipoUsuario"]));
            conexion.cerrar();

            return tipousuario;
        }
        public static TipoUsuarioTO TraerTipoUsuario(int idTipoUsuario)
        {
            //Acceso a la base de datos
            String query = "select * from TipoUsuario where idTipoUsuario=" + idTipoUsuario;
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                conexion.cerrar();
                return null;
            }
            reader.Read();
            TipoUsuarioTO tipou = new TipoUsuarioTO(Convert.ToInt32(reader["idTipoUsuario"]), Convert.ToString(reader["nombreTipoUsuario"]));
            
            conexion.cerrar();
            return tipou;
        }
    }


}
