﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using TO;

namespace DAO
{
    public class UsuarioDAO
    {
        private static Conexion conexion;

        static UsuarioDAO()
        {
            conexion = new Conexion();


        }

        public static List<UsuarioTO> consultarUsuarios()
        {
            //Acceso a la base de datos
            String query = "select * from usuario;";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            List<UsuarioTO> usuarios = new List<UsuarioTO>();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int idTipoUsuario = Convert.ToInt32(reader["idTipoUsuario"]);
                    TipoUsuarioTO tipousuario = TipoUsuarioDAO.TraerTipoUsuario(idTipoUsuario);

                    UsuarioTO usuario = new UsuarioTO(Convert.ToInt32(reader["idUsuario"]), Convert.ToString(reader["nombreUsuario"]), Convert.ToString(reader["contrasena"]), Convert.ToString(reader["correo"]).Trim(), Convert.ToBoolean(reader["esvip"]), tipousuario);
                    usuarios.Add(usuario);
                }
            }

            conexion.cerrar();
            return usuarios;
        }

        public static List<UsuarioTO> ListarTodoslosTecnicos()
        {
            //Acceso a la base de datos
            String query = "select * from Usuario where idTipoUsuario=1;";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            List<UsuarioTO> usuario = new List<UsuarioTO>();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int idTipoUsuario = Convert.ToInt32(reader["idTipoUsuario"]);
                    TipoUsuarioTO tipousuario = TipoUsuarioDAO.TraerTipoUsuario(idTipoUsuario);

                    UsuarioTO tecnico = new UsuarioTO(Convert.ToInt32(reader["idUsuario"]), Convert.ToString(reader["nombreUsuario"]), Convert.ToString(reader["contrasena"]), Convert.ToString(reader["correo"]), Convert.ToBoolean(reader["esvip"]), tipousuario);
                    usuario.Add(tecnico);
                }
            }

            conexion.cerrar();
            return usuario;
        }

        public static UsuarioTO TraerUsuarioCorreo(String correo)
        {
            //Acceso a la base de datos
            String query = "select * from Usuario where correo='" + correo + "'";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                conexion.cerrar();
                return null;
            }

            reader.Read();
            int idTipoUsuario = Convert.ToInt32(reader["idTipoUsuario"]);
            TipoUsuarioTO tipousuario = TipoUsuarioDAO.TraerTipoUsuario(idTipoUsuario);

            UsuarioTO usuariologin = new UsuarioTO(Convert.ToInt32(reader["idUsuario"]), Convert.ToString(reader["nombreUsuario"]), Convert.ToString(reader["contrasena"]), Convert.ToString(reader["correo"]), Convert.ToBoolean(reader["esvip"]), tipousuario);

            conexion.cerrar();
            return usuariologin;
        }

        public static UsuarioTO TraerUsuario(int idUsuario)
        {
            //Acceso a la base de datos
            String query = "select * from Usuario where idUsuario=" + idUsuario;
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                conexion.cerrar();
                return null;
            }

            reader.Read();
            int idTipoUsuario = Convert.ToInt32(reader["idTipoUsuario"]);
            TipoUsuarioTO tipousuario = TipoUsuarioDAO.TraerTipoUsuario(idTipoUsuario);
            UsuarioTO usuariologin = new UsuarioTO(Convert.ToInt32(reader["idUsuario"]), Convert.ToString(reader["nombreUsuario"]), Convert.ToString(reader["contrasena"]), Convert.ToString(reader["correo"]), Convert.ToBoolean(reader["esvip"]), tipousuario);

            conexion.cerrar();
            return usuariologin;
        }
        public static List<UsuarioTO> ListarTodoslosClientes()
        {
            //Acceso a la base de datos
            String query = "select * from Usuario where idTipoUsuario=2;";
            SqlCommand command = new SqlCommand(query, conexion.sqlConnection);
            List<UsuarioTO> usuario = new List<UsuarioTO>();

            conexion.abrir();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int idTipoUsuario = Convert.ToInt32(reader["idTipoUsuario"]);
                    TipoUsuarioTO tipousuario = TipoUsuarioDAO.TraerTipoUsuario(idTipoUsuario);
                    UsuarioTO tecnico = new UsuarioTO(Convert.ToInt32(reader["idUsuario"]), Convert.ToString(reader["nombreUsuario"]), Convert.ToString(reader["contrasena"]), Convert.ToString(reader["correo"]), Convert.ToBoolean(reader["esvip"]), tipousuario);
                    usuario.Add(tecnico);
                }
            }

            conexion.cerrar();
            return usuario;
        }

        public static void cambiarTecnicoAsignado(TicketTO ticket, UsuarioTO nuevo)
        {
            conexion.abrir();
            SqlCommand command = new SqlCommand("UPDATE Ticket SET idTecnico =" + nuevo.idUsuario + " WHERE idTicket =" + ticket.idTicket, conexion.sqlConnection);
            command.ExecuteNonQuery();
            conexion.cerrar();
        }

    }

}