﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DAO
{
    public class Conexion
    {
        public SqlConnection sqlConnection { get; }

        public Conexion()
        {
            sqlConnection = new SqlConnection("Data Source=.;Initial Catalog=HelpDesk;User Id=helpdesk;Password=helpdesk01");
        }

        public void abrir()
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Open();
            }
        }

        public void cerrar()
        {
            if (sqlConnection.State != ConnectionState.Closed)
            {
                sqlConnection.Close();
            }
        }

    }
}

