﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TO
{
    public class UsuarioTO
    {

        public int idUsuario { get; set; }
        public string nombreUsuario { get; set; }
        public string contrasena { get; set; }
        public string correo { get; set; }
        public Boolean esvip { get; set; }
        public TipoUsuarioTO tipoUsuario { get;  set; }

        public UsuarioTO(int idUsuario, string nombreUsuario, string contrasena, string correo, Boolean esvip, TipoUsuarioTO tipoUsuario)
        {
            this.idUsuario = idUsuario;
            this.nombreUsuario = nombreUsuario.Trim();
            this.contrasena = contrasena.Trim();
            this.correo = correo.Trim();
            this.esvip = esvip;
            this.tipoUsuario = tipoUsuario;
        }

    }
}
