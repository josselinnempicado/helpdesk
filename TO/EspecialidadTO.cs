﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TO
{
    public class EspecialidadTO
    {

        public int idEspecialidad { get ; set; }
        public string nombreEspecialidad { get ; set ; }

        public EspecialidadTO () { }

        public EspecialidadTO (int idEspecialidad, string nombreEspecialidad)
        {
            this.idEspecialidad = idEspecialidad;
            this.nombreEspecialidad = nombreEspecialidad.Trim();
        }
    }
}
