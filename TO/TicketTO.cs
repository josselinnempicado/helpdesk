﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TO
{
    public class TicketTO
    {
        public int idTicket { get; set; }
        public String asunto { get; set; }
        public DateTime ultimaActualizacion { get; set; }
        public DateTime fechaCreacion { get; set; }
        public DateTime fechaSolucion { get; set; }

        public UsuarioTO cliente { get; set; }
        public UsuarioTO tecnico { get; set; }
        public PrioridadTO prioridad { get; set; }
        public EstadoTO estado { get; set; }
        public EspecialidadTO especialidad { get; set; }
        public List<BitacoraTO> bitacoras { get; set; }

        public TicketTO(int idTicket, String asunto, DateTime ultimaActualizacion, DateTime fechaCreacion,
            DateTime fechaSolucion, UsuarioTO cliente, UsuarioTO tecnico, PrioridadTO prioridad, EstadoTO estado, EspecialidadTO especialidad)
        {
            this.idTicket = idTicket;
            this.asunto = asunto.Trim();
            this.ultimaActualizacion = ultimaActualizacion;
            this.fechaCreacion = fechaCreacion;
            this.fechaSolucion = fechaSolucion;
            this.cliente = cliente;
            this.tecnico = tecnico;
            this.prioridad = prioridad;
            this.estado = estado;
            this.especialidad = especialidad;
        }
    }
}
