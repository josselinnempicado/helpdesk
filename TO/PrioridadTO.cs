﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TO
{
    public class PrioridadTO
    {
        public int idPrioridad { get ; set; }
        public string nombrePrioridad { get ; set ; }

        public PrioridadTO() { }

        public PrioridadTO (int idPrioridad, string nombrePrioridad)
        {
            this.idPrioridad = idPrioridad;
            this.nombrePrioridad = nombrePrioridad.Trim();
        }
    }
}
