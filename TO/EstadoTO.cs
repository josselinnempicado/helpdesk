﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TO
{
    public class EstadoTO
    {

        public int idEstado { get ; set ; }
        public string nombreEstado { get ; set ; }

        public EstadoTO() { }

        public EstadoTO (int idEstado, string nombreEstado)
        {
            this.idEstado = idEstado;
            this.nombreEstado = nombreEstado.Trim();
        }
    }
}
