﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TO
{
    public class BitacoraTO
    {

        public int idBitacora { get ; set; }
        public DateTime fecha { get ; set ; }
        public string comentario { get; set ; }

        public UsuarioTO usuario { get; set; }
        public TicketTO ticket { get; set; }

        public BitacoraTO ( int idBitacora, DateTime fecha, string comentario, UsuarioTO usuario,TicketTO ticket)
        {
            this.idBitacora = idBitacora;
            this.fecha = fecha;
            this.comentario = comentario.Trim();
            this.usuario = usuario;
            this.ticket = ticket;
        }
    }
}
