﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TO
{
    public class TipoUsuarioTO
    {
        public string nombreTipoUsuario { get; set; }
        public int idTipoUsuario { get; set; }

        public TipoUsuarioTO () { }

        public TipoUsuarioTO(int idTipoUsuario, string nombreTipoUsuario)
        {
           this.idTipoUsuario = idTipoUsuario;
            this.nombreTipoUsuario = nombreTipoUsuario.Trim();
        }

    }
}
