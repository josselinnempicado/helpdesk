using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;

namespace ProyectoHelpDeskFinal.Pages
{
    public class PrimeraModel : PageModel
    {
        public void OnGet()
        {
            String correo = HttpContext.Session.GetString("_login");
            if (String.IsNullOrEmpty(correo))
            {
                Response.Redirect("/");
            }
        }
        public TO.UsuarioTO usuarioActual()
        {
            String correo = HttpContext.Session.GetString("_login");
            if (String.IsNullOrEmpty(correo))
            {
                return null;
            }
            else
            {
                return BL.UsuarioBL.TraerUsuarioCorreo(correo);
            }
        }
    }
}
