using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;


namespace WA.Pages
{
    public class SalirModel : PageModel
    {
        public void OnGet()
        {
            HttpContext.Session.SetString("_login", "");
            Response.Redirect("/");
        }

    }
}
