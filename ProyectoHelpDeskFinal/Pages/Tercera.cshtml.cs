using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;

namespace ProyectoHelpDeskFinal.Pages
{
    public class TerceraModel : PageModel
    {

        public bool esTicketNuevo { get; set; }

        public TO.TicketTO ticket { get; set; }

        public TerceraModel()
        {
            esTicketNuevo = true;
        }
        public void OnGet()
        {
            String correo = HttpContext.Session.GetString("_login");
            if (String.IsNullOrEmpty(correo))
            {
                Response.Redirect("/");
            }
        }
        public async Task<IActionResult> OnPostAsync()
        {

            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (String.Equals(this.Request.Form["idTicket"], null))
            {
                esTicketNuevo = true;
            }
            else
            {
                esTicketNuevo = false;
            }

            if (esTicketNuevo)
            {
                esTicketNuevo = false;
                ticket = BL.TicketBL.CrearNuevoTicket(this.Request.Form["asunto"], this.Request.Form["correo"], this.Request.Form["prioridad"], this.Request.Form["especialidad"], this.Request.Form["mensajeNuevo"], usuarioActual());
                if (String.Equals(this.Request.Form["cerrarTicket"], "true"))
                {
                    BL.TicketBL.cerrarTicket(ticket);
                }
                // Correo Tecnico
                String destinatario = ticket.tecnico.correo;
                String titulo = "Nuevo Ticket Asignado";
                String mensaje = "El usuario: " + ticket.cliente.nombreUsuario + " cre� un ticket nuevo n�mero: " + ticket.idTicket;
                BL.Correo.enviarCorreo(destinatario, titulo, mensaje);
                //Correo Cliente
                destinatario = ticket.cliente.correo;
                titulo = "Nuevo Ticket";
                mensaje = "Su ticket n�mero: " + ticket.idTicket + " ha sido creado.\nEste correo no puede ser contestado, si desea ver los detalles ingresar a la plataforma";
                BL.Correo.enviarCorreo(destinatario, titulo, mensaje);

                // ESVIP Administrador
                if (ticket.cliente.esvip)
                {
                    destinatario = BL.UsuarioBL.traerUusarioAdministrador().correo;
                    titulo = "Nuevo Ticket VIP";
                    mensaje = "El usuario VIP: " + ticket.cliente.nombreUsuario + " cre� un ticket nuevo n�mero: " + ticket.idTicket;
                    BL.Correo.enviarCorreo(destinatario, titulo, mensaje);
                }


            }
            else
            {
                int idTicket = Int32.Parse(this.Request.Form["idTicket"]);
                ticket = BL.TicketBL.TraerTicket(idTicket);

                if (!String.Equals(this.Request.Form["mensajeNuevo"], null))
                {
                    BL.BitacoraBL.CrearNuevaBitacora(this.Request.Form["mensajeNuevo"], usuarioActual(), ticket);
                }
                ticket.bitacoras = BL.BitacoraBL.TraerTodosLasBitacorasDeUnTicket(ticket);

                if (String.Equals(this.Request.Form["cerrarTicket"], "true"))
                {
                    BL.TicketBL.cerrarTicket(ticket);
                }

                String correo = this.Request.Form["correoTecnico"];
                if (!String.IsNullOrEmpty(correo))
                {
                    TO.UsuarioTO posibleNuevoTecnico = BL.UsuarioBL.TraerUsuarioCorreo(correo);
                    BL.UsuarioBL.revisarCambioTecnico(ticket, ticket.tecnico, posibleNuevoTecnico);
                }

                if (usuarioActual().tipoUsuario.Equals("Cliente"))
                {
                    String destinatario = ticket.tecnico.correo;
                    String titulo = "Nuevo mensaje en Ticket asignado";
                    String mensaje = "El cliente: " + ticket.cliente.nombreUsuario + " actualiz� el  ticket nuevo n�mero: " + ticket.idTicket;
                    BL.Correo.enviarCorreo(destinatario, titulo, mensaje);
                }
                if (usuarioActual().tipoUsuario.Equals("Tecnico"))
                {
                    String destinatario = ticket.cliente.correo;
                    String titulo = "Nuevo mensaje en su Ticket";
                    String mensaje = "El tecnico: " + ticket.tecnico.nombreUsuario + " actualiz� su ticket nuevo n�mero: " + ticket.idTicket + "\nEste correo no puede ser contestado, si desea ver los detalles ingresar a la plataforma.";
                    BL.Correo.enviarCorreo(destinatario, titulo, mensaje);
                }

                if (usuarioActual().tipoUsuario.Equals("Administrador"))
                {
                    String destinatario = ticket.tecnico.correo;
                    String titulo = "Nuevo mensaje en Ticket asignado";
                    String mensaje = "El Administrador actualiz� el ticket nuevo n�mero: " + ticket.idTicket;
                    BL.Correo.enviarCorreo(destinatario, titulo, mensaje);

                    destinatario = ticket.cliente.correo;
                    titulo = "Nuevo mensaje en Ticket";
                    mensaje = "El Administrador actualiz� el ticket nuevo n�mero: " + ticket.idTicket + "\nEste correo no puede ser contestado, si desea ver los detalles ingresar a la plataforma.";
                    BL.Correo.enviarCorreo(destinatario, titulo, mensaje);
                }
            }
            return Page();
        }

        public TO.UsuarioTO usuarioActual()
        {
            String correo = HttpContext.Session.GetString("_login");
            if (String.IsNullOrEmpty(correo))
            {
                return null;
            }
            else
            {
                return BL.UsuarioBL.TraerUsuarioCorreo(correo);
            }
        }
    }
}


