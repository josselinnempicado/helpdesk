﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ProyectoHelpDeskFinal.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            String correo = HttpContext.Session.GetString("_login");
            if (!String.IsNullOrEmpty(correo))
            {
                Response.Redirect("/Primera");
            }

        }

        public async Task<IActionResult> OnPostAsync()
        {
            String correo = this.Request.Form["correo"];
            if (!String.IsNullOrEmpty(correo))
            {
                TO.UsuarioTO posibleUsuario = BL.UsuarioBL.TraerUsuarioCorreo(correo);
                if (posibleUsuario !=null)
                {
                    String posibleContrasena = this.Request.Form["contrasena"];
                    if (posibleUsuario.contrasena.Equals(posibleContrasena))
                    {
                        HttpContext.Session.SetString("_login",correo);
                        return Redirect("/Primera");
                    }
                }
            }

            return Page();
        }

        public TO.UsuarioTO usuarioActual()
        {
            String correo = HttpContext.Session.GetString("_login");
            if (String.IsNullOrEmpty(correo))
            {
                return null;
            }
            else
            {
                return BL.UsuarioBL.TraerUsuarioCorreo(correo);
            }
        }
    } 
}
