﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TO;

namespace BL
{
    public class UsuarioBL
    {

        public List<UsuarioTO> consultarUsuarios()
        {
            return UsuarioDAO.consultarUsuarios();
        }

        public static List<TO.UsuarioTO> ListarTodoslosTecnicos()
        {
            return UsuarioDAO.ListarTodoslosTecnicos();
        }
        public static List<TO.UsuarioTO> ListarTodoslosClientes()
        {
            return UsuarioDAO.ListarTodoslosClientes();
        }
        public static  UsuarioTO TraerUsuarioCorreo(string correo)
        {
            return UsuarioDAO.TraerUsuarioCorreo(correo);
        }
        public static UsuarioTO traerUusarioAdministrador()
        {
            return UsuarioDAO.TraerUsuario(0);
        }

        public static void revisarCambioTecnico(TicketTO ticket, UsuarioTO previo, UsuarioTO nuevo) {
            if (previo.idUsuario != nuevo.idUsuario)
            {
                DAO.UsuarioDAO.cambiarTecnicoAsignado(ticket, nuevo);
                ticket.tecnico = nuevo;
            }
        }

    }
}
