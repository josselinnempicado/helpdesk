﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using TO;

namespace BL
{
    public class Correo
    {
        public static void enviarCorreo(String destinatario, String titulo, String mensaje)
        {

            UsuarioTO remitente = BL.UsuarioBL.traerUusarioAdministrador();
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);

            smtpClient.Credentials = new System.Net.NetworkCredential(remitente.correo, remitente.contrasena.Trim());
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(remitente.correo, "HelpDesk");
            mail.To.Add(new MailAddress(destinatario));

            mail.Subject = titulo;

            mail.Body = mensaje;

            System.Console.WriteLine("Enviando correo a: " + destinatario);

            smtpClient.Send(mail);
        }
        public static void enviarAlertas()
        {
            List<TicketTO> alertas = TicketBL.TraerTodosLosTicketsConAlerta();

            for (int i = 0; i < alertas.Count; i++)
            {
                TicketTO ticket = alertas.ElementAt(i);
                String destinatario = BL.UsuarioBL.traerUusarioAdministrador().correo;
                String titulo = "Alerta de ticket vencido: " + ticket.idTicket;
                String mensaje = "El ticket numero: " + ticket.idTicket + " tiene más de 6 días vencidos";
                enviarCorreo(destinatario,titulo,mensaje);
            }
        }
    }
}