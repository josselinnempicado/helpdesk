﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TO;

namespace BL
{
    public class EspecialidadBL
    {
        public static List<EspecialidadTO> consultarEspecialidades()
        {
            return DAO.EspecialidadDAO.consultarEspecialidades();
        }

        public static EspecialidadTO TraerEspecialidad(String nombreEspecialidad)
        {
            return DAO.EspecialidadDAO.TraerEspecialidad(nombreEspecialidad);
        }
        public static EspecialidadTO TraerEspecialidad(int idEspecialidad)
        {
            return DAO.EspecialidadDAO.TraerEspecialidad(idEspecialidad);
        }
    }
}
