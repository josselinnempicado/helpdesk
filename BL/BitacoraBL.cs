﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TO;
namespace BL
{
    public class BitacoraBL
    {
        public static BitacoraTO CrearNuevaBitacora(
            String comentario,
            UsuarioTO usuario,
            TicketTO ticket)
        {
            DateTime fecha = DateTime.Now;

            if (String.IsNullOrEmpty(comentario))
            {
                comentario = "(Sin Mensaje)";
            }
            return BitacoraDAO.CrearNuevaBitacora(fecha,comentario,usuario,ticket);
        }

        public static List<BitacoraTO> TraerTodosLasBitacorasDeUnTicket(TicketTO ticket)
        {
            return BitacoraDAO.TraerTodosLasBitacorasDeUnTicket(ticket);
        }
            
    }
}
