﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TO;

namespace BL
{
    public class TicketBL
    {
        private TicketDAO ticketDAO;

        public TicketBL()
        {
            ticketDAO = new TicketDAO();

        }
        public static List<TicketTO> TraerTodosLosTickets()
        {
            return TicketDAO.TraerTodosLosTickets();
        }
        public static TicketTO CrearNuevoTicket(String asunto, String correo, String nombrePrioridad, String idEspecialidadString, String mensajeNuevo, UsuarioTO usuarioActual)
        {
            DateTime ultimaActualizacion = DateTime.Now;
            DateTime fechaCreacion = DateTime.Now;
            TO.UsuarioTO cliente = BL.UsuarioBL.TraerUsuarioCorreo(correo);
            TO.EspecialidadTO especialidad = BL.EspecialidadBL.TraerEspecialidad(Int32.Parse(idEspecialidadString));
            TO.UsuarioTO tecnico = TicketDAO.TraerTecnicoConEspecialidad(especialidad);
            TO.PrioridadTO prioridad = BL.PrioridadBL.TraerPrioridad(nombrePrioridad);
            TO.EstadoTO estado = BL.EstadoBL.TraerEstado("Abierto");

            if (String.IsNullOrEmpty(asunto))
            {
                asunto = "(Sin Asunto)";
            }
            if (String.IsNullOrEmpty(mensajeNuevo))
            {
                mensajeNuevo = "(Sin Mensaje)";
            }

            TicketTO ticket = DAO.TicketDAO.CrearNuevoTicket(asunto, ultimaActualizacion, fechaCreacion, cliente, tecnico, prioridad, estado, especialidad);

            BitacoraBL.CrearNuevaBitacora(mensajeNuevo, usuarioActual, ticket);

            ticket.bitacoras = BitacoraBL.TraerTodosLasBitacorasDeUnTicket(ticket);

            return ticket;
        }
        public static TicketTO TraerTicket(int idticket)
        {
            return TicketDAO.TraerTicket(idticket);
        }
        public static List<TicketTO> TraerTodosMisTicketsCliente(int idCliente)
        {
            return TicketDAO.TraerTodosMisTicketsCliente(idCliente);
        }
        public static List<TicketTO> TraerTodosMisTicketsTecnico(int idTecnico)
        {
            return TicketDAO.TraerTodosMisTicketsTecnico(idTecnico);
        }
        public static String alerta(TicketTO ticket)
        {
            double dias = (DateTime.Now - ticket.fechaCreacion).TotalDays;
            if (dias < 3)
            {
                return "Verde";
            }
            else if (dias < 6)
            {
                return "Amarillo";
            }
            else
            {
                return "Rojo";
            }
        }
        public static List<TicketTO> TraerTodosLosTicketsConAlerta()
        {
            return TicketDAO.TraerTodosLosTicketsConAlerta();
        }
        public static void cerrarTicket(TicketTO ticket)
        {
            TicketDAO.cerrarTicket(ticket);
            EstadoTO estado = BL.EstadoBL.TraerEstado("Cerrado");
            ticket.estado = estado;
            ticket.fechaSolucion = DateTime.Now;
        }
    }
}