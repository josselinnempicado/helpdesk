﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TO;

namespace BL
{
    public class EstadoBL
    {
        public static EstadoTO TraerEstadoActual;

        public static EstadoTO TraerEstado(String nombreEstado)
        {
            return EstadoDAO.TraerEstado(nombreEstado);
        }
    }
}
