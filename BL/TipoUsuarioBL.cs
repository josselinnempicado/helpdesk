﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TO;

namespace BL
{
    public class TipoUsuarioBL
    {
        public static TipoUsuarioTO TraerTipoUsuario(String nombreTipoUsuario)
        {
            return TipoUsuarioDAO.TraerTipoUsuario(nombreTipoUsuario);
        }


    }
}

