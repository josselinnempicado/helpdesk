
USE Helpdesk


DROP TABLE IF EXISTS Bitacora
DROP TABLE IF EXISTS Ticket 
DROP TABLE IF EXISTS EspecialidadTecnico
DROP TABLE IF EXISTS Usuario
DROP TABLE IF EXISTS TipoUsuario
DROP TABLE IF EXISTS Estado
DROP TABLE IF EXISTS Prioridad
DROP TABLE IF EXISTS Especialidad
GO

CREATE TABLE TipoUsuario(
idTipoUsuario INT IDENTITY(0,1)  NOT NULL CONSTRAINT pk_TipoUsuario PRIMARY KEY,
nombreTipoUsuario CHAR(50) NOT NULL
)
GO

CREATE TABLE Usuario(
idUsuario INT IDENTITY(0,1)  NOT NULL  CONSTRAINT pk_Usuario PRIMARY KEY,
nombreUsuario CHAR(50) NOT NULL,
contrasena CHAR(50) NOT NULL,
correo CHAR(50) NOT NULL UNIQUE,
esvip BIT NOT NULL,
idTipoUsuario INT,
CONSTRAINT fk_UsuarioTipoUsuario FOREIGN KEY (idTipoUsuario) REFERENCES  TipoUsuario (idTipoUsuario)
)
GO


CREATE TABLE Estado(
idEstado INT IDENTITY(0,1)  NOT NULL CONSTRAINT pk_Estado PRIMARY KEY,
nombreEstado CHAR(50) NOT NULL
)
GO

CREATE TABLE Prioridad(
idPrioridad INT IDENTITY(0,1) NOT NULL CONSTRAINT pk_Prioridad PRIMARY KEY,
nombrePrioridad CHAR(50) NOT NULL
)
GO

CREATE TABLE Especialidad(
idEspecialidad INT IDENTITY(0,1)  NOT NULL CONSTRAINT pk_Especialidad PRIMARY KEY,
nombreEspecialidad CHAR(50) NOT NULL
)
GO

CREATE TABLE EspecialidadTecnico(
idUsuario INT,
CONSTRAINT fk_EspecialidadTecnicoUsuario FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
idEspecialidad  INT,
CONSTRAINT fk_EspecialidadTecnicoEspecialidad FOREIGN KEY (idEspecialidad ) REFERENCES Especialidad (idEspecialidad)
)
GO

CREATE TABLE Ticket (
idTicket INT IDENTITY(0,1)  NOT NULL CONSTRAINT pk_Ticket PRIMARY KEY,
asunto CHAR(50) NOT NULL,
ultimaActualizacion DATETIME NOT NULL,
fechaCreacion DATETIME NOT NULL,
fechaSolucion DATETIME,
idCliente INT NOT NULL,
idTecnico INT NOT NULL,
idPrioridad INT NOT NULL,
idEstado INT NOT NULL,
idEspecialidad INT NOT NULL,
CONSTRAINT fk_TicketUsuarioCliente FOREIGN KEY (idCliente) REFERENCES Usuario(idUsuario),
CONSTRAINT fk_TicketUsuarioTecnico FOREIGN KEY (idTecnico) REFERENCES Usuario(idUsuario),
CONSTRAINT fk_TicketPrioridad FOREIGN KEY (idPrioridad) REFERENCES Prioridad(idPrioridad),
CONSTRAINT fk_TicketEstado FOREIGN KEY (idEstado) REFERENCES Estado(idEstado),
CONSTRAINT fk_EspecialidadTecnico FOREIGN KEY (idEspecialidad) REFERENCES Especialidad(idEspecialidad)
)
GO

CREATE TABLE Bitacora(
idBitacora INT IDENTITY(0,1) NOT NULL CONSTRAINT pk_Bitacora PRIMARY KEY,
fecha DATETIME NOT NULL,
comentario TEXT NOT NULL,
idUsuario INT NOT NULL,
idTicket INT NOT NULL, 
CONSTRAINT fk_BitacoraUsuarioTecnico FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
CONSTRAINT fk_BitacoraTicket FOREIGN KEY (idTicket) REFERENCES Ticket(idTicket)
)
GO 


